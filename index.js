
var events = require("events");
var UserService = require("./UserService");
var ArticleService = require("./ArticleService");
var  ResponseMessage =require("./ResponseMessage") ;
var Engine =require('./Engine');
var ServiceHandler = require("./ServiceHandler") ;
var Rawer =  require("./Rawer");
var  Rx  = require("rx") ; 

 class AnyPaperSdk {


	constructor(serviceName){

		this.sendFrom =serviceName 
		this.serviceHandler = new ServiceHandler() ;
		this.engine = new Engine(serviceName,this.serviceHandler) ;
		this.serviceInstances ={} 
		this.createdOnce ={}
		this.serviceObservable = Rx.Observable.fromEvent(this,'serviceCreated');
		this.handleServiceUp();
		this.handleServiceDown();
		this.routeResponses();
	}

	createService(serviceName){
			let self = this;
			let userService ="UserService";
			let articleService= "ArticleService";
			let  readyEvent="serviceCreated"; 

			//console.log("createService",serviceName)
            
			switch (serviceName) {

				case userService : {
						
							self.serviceInstances[serviceName] = new UserService(self.sendFrom)
							
					break;
				}

				case articleService : {
				
							self.serviceInstances[serviceName]= new ArticleService(self.sendFrom)
						
	
					break;
				}
			}
			this.createdOnce[serviceName]= true ; 
			this.emit(readyEvent,serviceName); 



	}
	handleServiceUp(){

		let self = this;
		this.engine.on("serviceUp",function(serviceName){
				//console.log("serviceUp",serviceName)
				 let service = self.getServiceByName[serviceName]; 
				 //console.log("test",service==null)
					if(!service)
						self.createService(serviceName);

					

				
		})

	}

	handleServiceDown(){
		let self = this;	
		this.engine.on("serviceDown",function(serviceName){
			self.removeService(serviceName)		
		})

	}
	removeService(serviceName){
		let service = this.getServiceByName(serviceName); 

		if(service){
		
			service.emit("serviceDown")

			delete this.serviceInstances[serviceName] ; 
		}

	}


	getArticleService(callback){
		 this.createObserver("ArticleService",callback)
	}
	getUserService(callback){

		this.createObserver("UserService",callback)
	}

	createObserver(serviceNameParam,callback){
		let self = this;
			if(callback){	

				let service = self.serviceInstances[serviceNameParam]
				if(service)
					callback(service);

				let  observer = Rx.Observer.create(function(serviceName){

					 if(serviceName===serviceNameParam)
					 	callback(self.serviceInstances[serviceName])

				})
				this.serviceObservable.subscribe(observer) ; 


			}
	}
	

	routeResponses(){
		let self = this; 
		this.engine.on("Response",function(message){
			 	//console.log("response:",message);
				self.determineService(message);
		})
	}

	determineService(message)
	{		
			//console.log("determineService",message);
			let self =this ; 
			let serviceName = message.metadata.serviceName;
			let service =  this.getServiceByName(serviceName);


		if(service)
		{	let requestUuid = message.metadata.requestUuid;
			let content = message.content
			let response = new ResponseMessage(serviceName,self.sendFrom,requestUuid,content);
			service.emit("Response",response);
		 }		
	}

	addOperationHandler(handler){

			if(handler){
			 this.serviceHandler.addOperationHandler(handler)
			}
	}

	
	getServiceByName(serviceName){
	
			//console.log("getServiceByName:",serviceName,this.serviceInstances);
		return this.serviceInstances[serviceName];
	}
	getRawerService(callback){

		let rawer = new Rawer(this.sendFrom) ;

		this.serviceInstances["rawer"] = rawer
							
		callback(rawer); 

	}
	kill(){
		this.engine.stopHeartBeat();
	}
}

require('util').inherits(AnyPaperSdk, events.EventEmitter);
module.exports = AnyPaperSdk ; 