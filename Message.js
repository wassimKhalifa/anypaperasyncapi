

var uuid = require("uuid/v4"); 
var _=require('underscore');

module.exports = class Message {

	 constructor(content,serviceName){	 
	 	this.serviceName = serviceName; 	
	 	this.messageUuid = uuid() ;
        this.createdAt = Math.floor(Date.now()/1000);
	 	this.content = content ;
	 	
	 }

	getJsonFormat(){
			let serviceName = this.serviceName; 
		 	let messageUuid =this.messageUuid;
		 	let createdAt = this.createdAt; 
			let message={ 
					metadata:{
						messageUuid,
						createdAt,
						serviceName
					},
					content:this.content
			}
			return message ; 
	}
	toString(){
		let jsonFromat = this.getJsonFormat(); 
		return(JSON.stringify(jsonFromat))
	}

}