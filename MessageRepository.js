
var redis =require("redis");
var config = require("./Config")
module.exports = class MessageRepository{

   constructor(){
    this.messageStorage = redis.createClient(config.REDIS_HOST);
    }

	 storeOutputMessage (key,message,callback) 
  	{
         if (message && key)
          this.messageStorage.hset(key,message.messageUuid,message.toString(),callback ) 
 
  	}

   	storeInputMessage (key,message,callback){
      
         if( message && key)  
         this.messageStorage.hset(key,message.messageUuid,message.toString(),callback ) ;    
   	}


	  getOutputMessage (key,messageUuid , callback) {
        if(messageUuid&&key)
        this.messageStorage.hget(key,messageUuid,callback) ; 
  	}

  	getInputMessage (key,messageUuid ,callback){
      	if( messageUuid&&key)
        this.messageStorage.hget(key,messageUuid,callback) ;
  	}

  	removeOutputMessage (key,messageUuid,callback){

          if(messageUuid&&key )
          this.messageStorage.hdel(key,messageUuid,callback) ; 
        
  	}
   	removeInputMessage (key,messageUuid,callback){
          if(messageUuid&&key )
          this.messageStorage.hdel(key,messageUuid,callback) ; 
    }

}