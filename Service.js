'use strict'
var Promise =require("promise") ;
var redis =require("redis");
var config = require('./Config')
var MessageRepository = require('./MessageRepository');
var RequestMessage = require("./RequestMessage");
var RedisMessagingHelpers = require("./RedisMessagingHelpers");
var events =require("events") ; 

const  OPERATION_TIME_OUT = 5000; 
 class Service{


	constructor(sendTo,sendFrom){
		
		this.sendFrom = sendFrom;
		this.sendTo = sendTo; 
		this.isActive = true; 
		this.outputMessagesKey ="outputMessages:"+this.sendFrom ; 
		this.inputMessagesKey ="inputMessages:"+this.sendFrom ; 
		this.callbackRegistry = {};
		this.messageStorage= redis.createClient(config.REDIS_HOST);
		this.messageRepository =  new MessageRepository() ; 
		this.messagingHelpers = new RedisMessagingHelpers();

		this.handleResponses();
	}
	isServiceActive(){
		return this.isActive ;
	}
	isReceiverActive(receiverName){

	}
	sendRequest(request){
		let self = this ; 
		if(request)
			self.messagingHelpers.publishMessage(request.receiverChannel,request.toString()); 
	}
	execOperation(content,callback){

			
			let self = this ; 
			if(content)
			 {
			 	 
			 	let request = self.createRequest(content);
			 	let messageUuid = request.messageUuid ; 
			 	self.messageRepository.storeOutputMessage(self.outputMessagesKey,request,function(error,result){
			 		
			 		if(result)
			 		{  	
			 			if(callback)
			 			self.addCallbackToRegistry(messageUuid,callback);
			 			
			 			self.sendRequest(request) ; 
			 		}
			 	})
			 }
	

	}
	createRequest(content){
		let request = new RequestMessage(this.sendFrom,this.sendTo,content) ;
		return request ; 
	}
	addCallbackToRegistry(messageUuid,callback){
		let self = this; 
		if(callback && messageUuid){
			let operationTimeOut ;
			if(this.sendTo !=="RawerService")
			operationTimeOut =self.createOperationTimeOut(messageUuid)
			

			 this.callbackRegistry[messageUuid] = {
				callback,
				operationTimeOut
			}

		}
	}

	createOperationTimeOut(messageUuid){
		let self = this; 
		return  setTimeout(function(){
					self.callbackRegistry[messageUuid].callback(new Error("service not reponding"), null ) ; 
					delete  self.callbackRegistry[messageUuid]; 
				},OPERATION_TIME_OUT); 
	}
	handleResponses(){
		let self =this ;
		this.on("Response",function(message){
			let requestUuid = message.requestUuid;
			let operation = self.callbackRegistry[requestUuid];
		
					if(operation){
				self.messageRepository.storeInputMessage(self.inputMessagesKey,message, function(error,result){

												
											
											let content = message.content	
											

													let isError = content.error
													let operationTimeOut = operation.operationTimeOut ; 
													if(operationTimeOut)
														clearTimeout(operationTimeOut)
													if(isError){
														operation.callback(new Error(content.message),null);
													}else{
														operation.callback(null,content); 
													}
               										
               										delete self.callbackRegistry[requestUuid] ; 
                								
				})
			}
		})
	}
}

require('util').inherits(Service, events.EventEmitter);
module.exports = Service ; 

