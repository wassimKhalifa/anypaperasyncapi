'use strict' ;
var Service =require('./Service')
module.exports=class UserService extends Service{

	constructor(sendFrom){
		super("UserService",sendFrom) ; 
	}

	getUserById(userId,callback){
		
		let operationName = this.getUserById.name;
		let args = {
			userId,
			operationName
		}
		this.execOperation(args,callback) ;
	}
	getAllUsers(callback){
		let operationName = this.getAllUsers.name;
		let args = {
			operationName
		}
		this.execOperation(args,callback) ;
	}
	createUser(user, callback){
		let operationName = this.createUser.name;
		let args = {
			user,
			operationName
		}
		this.execOperation(args,callback) ;
	}
	getUserByUuid(userUuid , callback){
		let operationName = this.getUserByUuid.name;
		let args = {
			userUuid,
			operationName
		}
		this.execOperation(args,callback) ;
	}
	getUserById(userId,callback){
		let operationName = this.getUserById.name; 
		let args={
			userId,
			operationName
		}
		this.execOperation(args,callback) ;
	}
	getUserByLogin(login, callback){
		let operationName = this.getUserByLogin.name; 
		let args={
			login,
			operationName
		}
		this.execOperation(args,callback) ;
	}
	authenticate(userLogin ,userPassword, callback){
		let operationName = this.authenticate.name; 
		let args={
			userLogin,
			userPassword,
			operationName
		}
		this.execOperation(args,callback) ;
	}
	addArticleToBookMark(userUuid,articleUuid,callback){
		let operationName = this.addArticleToBookMark.name; 
		let args={
			userUuid,
			articleUuid,
			operationName
		}
		this.execOperation(args,callback) ;
	}
	removeArticleFromBookMark(userUuid,articleUuid,callback){

		let operationName = this.removeArticleFromBookMark.name; 
		let args={
			userUuid,
			articleUuid,
			operationName
		}
		this.execOperation(args,callback) ;
	
	}
	getUserBookMark(userUuid,callback){
		let operationName = this.getUserBookMark.name; 
		let args={
			userUuid,
			operationName
		}
		this.execOperation(args,callback) ;
	
	}
}
