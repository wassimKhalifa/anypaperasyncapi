var redis  = require('redis') ; 
var uuid = require("uuid/v4"); 
var config =require("./Config");
module.exports = class RedisMessagingHelpers{
    
  constructor(){
      this.publisher = redis.createClient(config.REDIS_HOST) ;
      this.subscriber =redis.createClient(config.REDIS_HOST) ;
  }

  publishMessage (channel , message) {
        if(channel&&message) 
            this.publisher.publish(channel,message.toString()) ;      
  }

  subscribeToChannel (channels){

        if(channels)
         this.subscriber.subscribe(channels) ;   
  }
  onMessage (messageHandler){
      if(messageHandler)
         this.subscriber.on("message",messageHandler)
  }

}