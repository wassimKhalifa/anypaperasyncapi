

var uuid = require("uuid/v4"); 
var Message= require('./Message');
var _=require('underscore');

module.exports = class ResponseMessage extends Message {

	 constructor(senderName,receiverName,requestUuid,content){
	 	super(content,senderName);
	 	this.requestUuid=requestUuid;
	 	this.receiverChannel = receiverName;
	 }

	getJsonFormat(){
			
			let message= super.getJsonFormat();
			let metadata = message.metadata ;
			metadata.requestUuid = this.requestUuid; 
			metadata.serviceName=this.serviceName;
			metadata.receiverChannel=this.receiverChannel;
			return message ; 
	}
	toString(){
		let jsonFromat = this.getJsonFormat(); 
		return(JSON.stringify(jsonFromat))
	}

}