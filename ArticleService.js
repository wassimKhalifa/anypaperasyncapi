'use strict' ;
var Service =require('./Service')
module.exports=class ArticleService extends Service{

	constructor(sendFrom){
		super("ArticleService",sendFrom) ; 
	}
	createSnippet(snippet,callback){
		let operationName = this.createSnippet.name;
		let args ={
			operationName,
			snippet
		}
		this.execOperation(args,callback) ;
	}
    getSnippetContentByUuid(snippetUuid,callback){ 
        
        let operationName = this.getSnippetContentByUuid.name;
		let args ={
			operationName,
			snippetUuid
		}
		this.execOperation(args,callback) ;
          
    }
    getSnippetsByPageUri(pageUri,callback){ 
        
        let operationName = this.getSnippetsByPageUri.name;
		let args ={
			operationName,
			pageUri
		}
		this.execOperation(args,callback) ;
    }
    getSnippetByUuid(snippetUuid,callback){ 
            let operationName = this.getSnippetByUuid.name;
		let args ={
			operationName,
			snippetUuid
		}
		this.execOperation(args,callback) ;
          
    }
	getArticleByUri(articleUri,callback){
		let operationName = this.getArticleByUri.name;
		let args ={
			operationName,
			articleUri
		}
		this.execOperation(args,callback) ;
	}
	getArticleByUuid(articleUuid,callback){
		let operationName = this.getArticleByUuid.name;
		let args ={
			operationName,
			articleUuid
		}
		this.execOperation(args,callback) ;
	}
	checkArticleByUri(articleUri,callback){
		let operationName = this.checkArticleByUri.name;
		let args ={
			operationName,
			articleUri
		}
		this.execOperation(args,callback) ;
	}
	getArticleContentByUuid(articleUuid,callback){
		let operationName = this.getArticleContentByUuid.name;
		let args ={
			operationName,
			articleUuid
		}
		this.execOperation(args,callback) ;
	}	
	getArticles(articleUuids,callback){
		let operationName = this.getArticles.name;
		let args ={
			operationName,
			articleUuids
		}
		this.execOperation(args,callback) ;
	}	
	getAllArticles(callback){
			let operationName = this.getAllArticles.name;
		let args ={
			operationName
		}
		this.execOperation(args,callback) ;

	}
	
}