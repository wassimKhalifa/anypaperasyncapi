
var _  =require("underscore");

module.exports = class ServiceHandler{

	constructor(){
		this.operationHandlers = {};
	}


	executeOperation(opeartionName,message,callback){
			
			let handler = this.operationHandlers[opeartionName] ; 
			if(handler && _.isFunction(handler))
				handler(message,callback); 
			else 
				callback(new Error("unable to execute operation"),null)
			
	}
	addOperationHandler(operationHandler){
		let self = this;
		if(operationHandler && _.isFunction(operationHandler))
				getFunctionName(operationHandler,function(error,funcName){
					 self.operationHandlers[funcName] = operationHandler ;  
				})
	}




}

var getFunctionName =function (func , callback){

    if(func && _.isFunction(func)){
      
      var funcName =func.toString(); 
          funcName =  funcName.substring( funcName.indexOf("function")+"function".length, funcName.indexOf("("))
          funcName = funcName.trim();    

      if(funcName )
        { 
        callback(null,funcName)
        return ;
        }
   }else{
    
      callback(new Error("unable to get function name"),null)
   }

}