

var uuid = require("uuid/v4"); 
var Message= require('./Message');
var _=require('underscore');

module.exports = class RequestMessage extends Message {

	 constructor(senderName,receiverName,content){
	 	super(content,senderName);
	 	this.receiverChannel =receiverName;
	 	
	 }

	getJsonFormat(){
			let message= super.getJsonFormat();
			let metadata=message.metadata; 
			metadata.receiverChannel=this.receiverChannel;
			return message ;  
	}
	toString(){
		let jsonFromat = this.getJsonFormat(); 
		return(JSON.stringify(jsonFromat))
	}

}