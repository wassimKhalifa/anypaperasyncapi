
var RedisMessagingHelpers = require("./RedisMessagingHelpers");
var Message =  require("./Message");
var events =  require("events"); 
var ResponseMessage = require("./ResponseMessage")


const HEART_BEAT_INTERVAL = 100;
const  HEART_BEAT_CHANNEL ="heartbeat";


class Engine  {

 	constructor(serviceName, serviceHandler){
 		this.channels=[HEART_BEAT_CHANNEL,serviceName]
 		this.serviceName =  serviceName; 
 		this.messagingHelpers = new RedisMessagingHelpers();
 		this.serviceHandler = serviceHandler; 
 		this.activeServices ={} ;
 		this.heartbeats
 		this.initiate();	
 	}

 	initiate(){
 		this.subscribeToChannels();
 		this.startHeartBeat();
 		this.handleMessages();
 	}

 	subscribeToChannels(){
 		let self = this; 
 		this.channels.forEach(function(channel){
 			self.messagingHelpers.subscribeToChannel(channel);
 		})
 	}
 	startHeartBeat(){
 		let self = this; 
 		 this.heartbeats= setInterval( function(){
             let brodcastMessage = new Message({serviceName:self.serviceName},self.serviceName)
          self.messagingHelpers.publishMessage(HEART_BEAT_CHANNEL,brodcastMessage) ; 
          },HEART_BEAT_INTERVAL) 
 	}
 	stopHeartBeat(){
 		if(this.heartbeats)
 			clearInterval(this.heartbeats)
 	}
 	handleMessages(){
 		let self = this;
 		this.messagingHelpers.onMessage(function(channel,stringMessage){
 		
 			try{
 				
 				let message = JSON.parse(stringMessage);
 				if(channel===HEART_BEAT_CHANNEL)
 					 self.handleServicesHearBeats(message);
 				else{

 					if(channel===self.serviceName)
 						self.handleServiceMessages(message);
 					else
 						self.handleBroadcastMessages(channel,message);
 				}
 			}catch(error){
 				//console.log(error)
 			}

 		})
 	}

 	handleServiceMessages(message){
 			let self = this;
 			let isResponse = message.metadata.requestUuid;

 			if(!isResponse){	
 				let operationName = message.content.operationName; 
 				this.serviceHandler.executeOperation(operationName,message.content,
 													function(error,result){
 														self.handleOperationResult(error,message,result)
 													}); 
 			}else		
 				{	
 					this.emit("Response",message);
 				}
 			
 	}
   	handleOperationResult(error,request,result){	
   			
 			let message ;									 		
   		if(result)
   			message = result ; 
      if(error)
         message = {error:true,message:error.message}
      this.respondToRequest(request,message)

   	}
   	respondToRequest(request,content){
   		
   		if(request && content){
   			let senderName = this.serviceName;
   			let receiverName = request.metadata.serviceName;
   			let requestUuid = request.metadata.messageUuid ; 
   			let response = new ResponseMessage(senderName,receiverName,requestUuid,content) ;			
   			this.messagingHelpers.publishMessage(receiverName,response) ; 
   		}
   	}
 	handleBroadcastMessages(channel,message){

 	}

 	handleServicesHearBeats(message){	

 		let serviceName = message.content.serviceName;

 		if(serviceName!=this.serviceName)
 				this.resetActiveService(serviceName);
 	}
 	resetActiveService(serviceName){
 		let self =this;
   		let serviceTimeOut = this.activeServices[serviceName]

   		if(serviceTimeOut)
  	    	clearTimeout(serviceTimeOut);
  		else
  			this.emit("serviceUp",serviceName);

     	this.activeServices[serviceName]= setTimeout(function(){
            delete self.activeServices[serviceName]  ; 
 			self.emit("serviceDown",serviceName);
    	  },HEART_BEAT_INTERVAL*5);
 	}




}
require('util').inherits(Engine, events.EventEmitter);
module.exports =Engine