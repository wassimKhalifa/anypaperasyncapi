

var REDIS_HOST = (process.env.ANYPAPER_BUS_PORT)?process.env.ANYPAPER_BUS_PORT:"redis://127.0.0.1:6379"

console.log("debug:\n ENV:",process.env.ANYPAPER_BUS_PORT,"\nREDIS_HOST:",REDIS_HOST)
module.exports = {
	REDIS_HOST
}